import React, {useState, useEffect} from 'react';
import logo from './logo.svg';
import './App.css';

function App() {

  const [field, setField] = useState(matrix(4,4))

  function getRandomInt(max) {
    return Math.floor( Math.random() * max / 2 ) * 2;
  }

  function getRandomPosition() {
    return [Math.floor(Math.random() * Math.floor(4)), Math.floor(Math.random() * Math.floor(4))]
  }

  function matrix(m, n) {
    var result = []
    for(var i = 0; i < n; i++) {
        result.push(new Array(m).fill(0))
    }
    return result
  }


  const generateValue = () => {
    const position = getRandomPosition();
    const value  = getRandomInt(3)
    let area = [...field]
    area[position[0]][position[1]] = area[position[0]][position[1]] === 0 ? value : area[position[0]][position[1]];
    setField(area)
  }

  useEffect(() => {
    console.log('field', field)
  }, [field])

  useEffect(() => {
    generateValue();
    generateValue();
    generateValue();
  }, [])



  const move = event => {
    switch (event.key) {
      case 'ArrowUp':
        let area = [...field]
      field.forEach((row, rowIndex) => {
        row.forEach((column, columnIndex) => {
          if(field[rowIndex][columnIndex] !== 0) {
            console.log(rowIndex)
            if(area[rowIndex-1][columnIndex] === 0 && rowIndex !== 0) {
              area[rowIndex-1][columnIndex] = area[rowIndex][columnIndex]
              area[rowIndex][columnIndex] = 0
            }
          }
        })
      })
      setField(area);
      break;
      case 'ArrowDown':
      console.log('Down')
      break;
      case 'ArrowLeft':
      console.log('Left')
      break;
      case 'ArrowRight':
      console.log('Right')
      break;
    
      default:
        break;
    }
  }

  // console.log(field)

  return (
    <Field area={field} move={move}/>
  );
}

const Field = ({area, move}) => (
  <div className="App" onKeyDown={move} tabIndex="0">
    {area.map(row => {
      return (
        <div className="row">
          {row.map(field => {
            return(<div className="column">{field !== 0 && field}</div>)
          })}
        </div>
      )
    })}
  </div>
);

export default App;
